// 1st method works slowly with big n
const fib = (n) => (n === 0 || n === 1) ? 1 : (n > 1) ? fib(n-1) + fib(n-2) : 'Oops! Something went wrong!';
console.log(fib(11)); //144

//2nd method works well with big n
const fib1 = (m) => {
  let f1 = 1;
  let f2 = 1;
  if (m === 0 || m === 1) {
    console.log(1);
  } else if (m > 1) {
    for (let i = 2; i <= m; i++) {
      let f3 = f1 + f2;
      f1 = f2;
      f2 = f3;
    }
    console.log(f2);
  } else {
    console.log('Oops! Something went wrong!');
  }
}

fib1(110); //7.049252476708914e+22
